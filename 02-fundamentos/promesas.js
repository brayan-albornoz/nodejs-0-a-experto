let empleados = [{
    id: 1,
    nombre: 'Brayan'
}, {
    id: 2,
    nombre: 'Alexander'
}, {
    id: 3,
    nombre: 'Diego'
}];

let salarios = [{
    id: 1,
    salario: 1000
}, {
    id: 2,
    salario: 2000
}];

let getEmpleado = (id) => {

    return new Promise((resolve, reject) => {
        let empleadoDB = empleados.find(empleado => {
            return empleado.id === id;
        });

        if (!empleadoDB) {
            reject(`No existe un empleado con el ID: ${id}`);
        } else {
            resolve(empleadoDB);
        }
    });
}

let getSalario = (empleado) => {
    return new Promise((resolve, reject) => {
        let salarioDB = salarios.find(salario => {
            return salario.id === empleado.id;
        });

        if (!salarioDB) {
            reject(`No se encontró un salario para el usuario ${empleado.nombre}`);
            reject(`No se encontró un salario para el usuario ${empleado.nombre}`);
            reject(`No se encontró un salario para el usuario ${empleado.nombre}`);
            reject(`No se encontró un salario para el usuario ${empleado.nombre}`);
            // console.log('Otra línea después del reject.');
        } else {
            resolve({
                nombre: empleado.nombre,
                salario: salarioDB.salario,
                id: empleado.id
            });
        };
    });
}

// getEmpleado(3).then(empleado => {
//     // console.log('Empleado de BD', empleado);

//     getSalario(empleado).then(resp => {
//         console.log(`El salario de ${resp.nombre} es de $${resp.salario}`);
//     }, err => console.log(err));

// }, (err) => console.log(err));

console.log('------------------------');

getEmpleado(10).then(empleado => {
        return getSalario(empleado);
    })
    .then(resp => {
        console.log(`El salario de ${resp.nombre} es de $${resp.salario}`);
    })
    .catch(err => {
        console.log(err);
    });